import React, { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import Button from "../components/Button";
import { gameActions } from "../features/gameez/slices/gameInitSlices";
import {
  gameQuizActions,
  selectCurrentQuestionIndex,
  selectQuestions,
  selectScore,
} from "../features/gameez/slices/gameSlices";

export const GamePage = () => {
  const [timeLeft, setTimeLeft] = useState(60);

  const currentQuestionIndex = useAppSelector(selectCurrentQuestionIndex);

  const currentQuestion =
    useAppSelector(selectQuestions)[currentQuestionIndex].question;

  const score = useAppSelector(selectScore);

  const dispatch = useAppDispatch();

  const answerQuestion = (answer: string) => {
    dispatch(gameQuizActions.answerQuestion(answer));
  };

  const endGame = () => {
    dispatch(gameActions.finnishGame());
  };

  useEffect(() => {
    const interval = setInterval(() => {
      setTimeLeft((prev) => prev - 1);
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <>
      <div className="flex flex-col items-center relative">
        <p className="h-20 w-20 flex justify-center items-center border-8 border-purple-500 rounded-full my-4 text-3xl text-purple-500">
          {timeLeft}
        </p>
        <p className="absolute top-4 left-4 text-2xl text-purple-500">
          {score}
        </p>
        <p className="absolute top-4 right-4 text-2xl text-purple-500">
          {currentQuestionIndex}/10
        </p>
        <p
          dangerouslySetInnerHTML={{ __html: currentQuestion }}
          className="p-7 bg-white rounded shadow "
        ></p>
        <div className="flex justify-between w-96 mt-8">
          <Button onClick={() => answerQuestion("True")}>True</Button>
          <Button onClick={() => answerQuestion("False")}>False</Button>
        </div>
      </div>
      <div className="absolute bottom-4 right-4">
        <Button onClick={endGame} type="error">
          Quit Game
        </Button>
      </div>
    </>
  );
};
