import * as React from "react";
import { useAppSelector } from "../app/hooks";
import { selectStage } from "../features/gameez/slices/gameInitSlices";
import { myConstants } from "../utils/constants";
import { EndGamePage } from "./EndGamePage";
import { FetchingPage } from "./FetchingPage";
import { GamePage } from "./GamePage";
import { StartGamePage } from "./StartGamePage";

export default function MainPage() {
  const currentStage = useAppSelector(selectStage);
  let displayedPage;

  switch (currentStage) {
    case myConstants.START_GAME:
      displayedPage = <StartGamePage />;
      break;
    case myConstants.FETCHING_GAME_DATA:
      displayedPage = <FetchingPage />;
      break;
    case myConstants.GAME:
      displayedPage = <GamePage />;
      break;
    case myConstants.END_GAME:
      displayedPage = <EndGamePage />;
      break;
    default:
      break;
  }
  return (
    <div className="font-mono bg-purple-50 min-h-screen ">
      <h1 className="bg-purple-500 text-white p-4 text-2xl text-center uppercase">
        Redux Saga Quiz Game
      </h1>
      {displayedPage}
    </div>
  );
}
