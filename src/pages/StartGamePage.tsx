import React, { useState } from "react";
import { useAppDispatch } from "../app/hooks";
import Button from "../components/Button";
import { gameActions } from "../features/gameez/slices/gameInitSlices";
import { myConstants } from "../utils/constants";
export const StartGamePage = () => {
  
  const [username, setUsername] = useState("Raul");
  const dispatch = useAppDispatch();

  const startGameHandler = () => {
    dispatch(
      gameActions.startGame({
        stage: myConstants.FETCHING_GAME_DATA,
        username: username,
      })
    );
  };

  return (
    <div className="flex flex-col justify-center items-center mt-80">
      <input
        value={username}
        onChange={(e) => setUsername(e.target.value)}
        placeholder="Your Name..."
        className="py-2 px-4 outline-none rounded shadow w-64 mb-6"
      />
      <Button onClick={startGameHandler}>Start Game</Button>
    </div>
  );
};
