import React from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import Button from "../components/Button";
import { gameActions } from "../features/gameez/slices/gameInitSlices";
import {
  selectAnswers,
  selectScore,
} from "../features/gameez/slices/gameSlices";

export const EndGamePage = () => {
  const dispatch = useAppDispatch();
  const answers = useAppSelector(selectAnswers);
  const score = useAppSelector(selectScore);
  const restartHandlers = () => {
    dispatch(gameActions.restartGame());
  };

  return (
    <div className="flex flex-col items-center">
      <h1 className="text-3xl text-purple-500 my-4">Game Over</h1>
      <h2 className="text-2xl mb-4">
        Your score was <span className="text-purple-500">{score}</span> out of
        10.
      </h2>
      <Button onClick={restartHandlers}>Try Again</Button>
      <div className="mt-4 p-4">
        {answers.map((answer) => (
          <div
            key={answer.question}
            className="border-b-2 border-purple-500 flex justify-between bg-white"
          >
            <span
              dangerouslySetInnerHTML={{ __html: answer.question }}
              className="mr-4 p-2"
            ></span>
            <span
              className={`p-2 ${
                answer.correctAnswer === answer.answer
                  ? "text-green-500"
                  : "text-red-500"
              }`}
            >
              {answer.answer}
            </span>
          </div>
        ))}
      </div>
    </div>
  );
};
