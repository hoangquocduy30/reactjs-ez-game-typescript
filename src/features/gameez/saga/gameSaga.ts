import { gameActions } from "./../slices/gameInitSlices";
import { race, take, delay, put } from "@redux-saga/core/effects";
import { gameQuizActions } from "../slices/gameSlices";

function* answersSaga() {
  for (let i = 0; i < 10; i++) {
    yield take(gameQuizActions.answerQuestion.type);

    yield put(gameQuizActions.nextQuestion());
  }
}

export default function* gameSaga() {
  while (true) {
    yield take(gameQuizActions.featchQuestionsSuccess.type);
    yield race({
      delay: delay(60000),
      done: answersSaga(),
    });
    yield put(gameActions.finnishGame());
  }
}
