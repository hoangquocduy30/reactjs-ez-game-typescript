import {
  all,
  take,
  fork,
  call,
  put,
  delay,
  cancel,
  CallEffect,
} from "redux-saga/effects";
import featchQuizApi from "../../../api/featchQuizApi";
import { listResponse } from "../../../model/common";
import { Quiz } from "../../../model/quiz";
import { gameActions } from "../slices/gameInitSlices";
import { gameQuizActions } from "../slices/gameSlices";

function* fetchQuestionsSaga() {
  try {
    yield delay(1000);
    const responseList: listResponse<Quiz>[] = yield all([
      call(featchQuizApi.getAll),
    ]);
    yield put(gameQuizActions.featchQuestionsSuccess(responseList[0]));
  } catch (error) {
    yield put(gameQuizActions.featchQuestionsFall());
  }
}

function* cancelFetchQuiz(forkedSage: any) {
  while (true) {
    yield take(gameActions.cancelGame.type);
    yield cancel(forkedSage);
  }
}

export default function* startGameSaga() {
  while (true) {
    yield take(gameActions.startGame.type);

    const forkedSage: Generator<CallEffect<true>> = yield fork(
      fetchQuestionsSaga
    );

    yield fork(cancelFetchQuiz, forkedSage);
  }
}
