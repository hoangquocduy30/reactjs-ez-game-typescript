import { myConstants } from "../../../utils/constants";

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../../app/store";
import { gameQuizActions } from "./gameSlices";

export interface GameInitState {
  stage: myConstants;
  username: string;
}

const initialState: GameInitState = {
  stage: myConstants.START_GAME,
  username: "",
};

const gameInitSlices = createSlice({
  name: "gameState",
  initialState,
  reducers: {
    startGame(state, action: PayloadAction<GameInitState>) {
      state.stage = action.payload.stage;
      state.username = action.payload.username;
    },
    cancelGame(state) {
      state.stage = myConstants.START_GAME;
    },
    finnishGame(state) {
      state.stage = myConstants.END_GAME;
    },
    restartGame(state) {
      state.stage = myConstants.START_GAME;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(gameQuizActions.featchQuestionsSuccess, (state, action) => {
      state.stage = myConstants.GAME;
    });
    builder.addCase(gameQuizActions.featchQuestionsFall, (state, action) => {
      state.stage = myConstants.START_GAME;
    });
  },
});

// Actions
export const gameActions = gameInitSlices.actions;

// // Selectors
export const selectStage = (state: RootState) => state.gameState.stage;
export const selectUsername = (state: RootState) => state.gameState.username;

// Reducer

const gameInitReducer = gameInitSlices.reducer;
export default gameInitReducer;
