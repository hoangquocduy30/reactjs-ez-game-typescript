import { myConstants } from "../../../utils/constants";

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../../app/store";
import { Quiz } from "../../../model/quiz";
import { gamequiz } from "../../../model/gamequiz";
import { listResponse } from "../../../model/common";

export interface GameInitState {
  stage: myConstants;
  username: string;
}

export interface Answers {
  question: string;
  answer: string;
  correctAnswer: string;
  isCorrect: boolean;
}

const initialState: gamequiz = {
  questions: [],
  error: "",
  score: 0,
  currentQuestionIndex: 0,
  answers: [],
};

const gameSlices = createSlice({
  name: "quiz",
  initialState,
  reducers: {
    featchQuestionsSuccess(state, action: PayloadAction<listResponse<Quiz>>) {
      state.questions = action.payload.results;
      state.score = 0;
      state.currentQuestionIndex = 0;
      state.answers = [];
    },
    featchQuestionsFall(state) {
      state.error = "error";
    },

    answerQuestion(state, action: PayloadAction<string>) {
      const currentQuestion = state.questions[state.currentQuestionIndex];
      state.score += action.payload === currentQuestion.correct_answer ? 1 : 0;
      state.answers.push({
        question: currentQuestion.question,
        answer: action.payload,
        correctAnswer: currentQuestion.correct_answer,
        isCorrect: action.payload === currentQuestion.correct_answer,
      });
    },
    nextQuestion(state) {
      state.currentQuestionIndex += 1;
    },
  },
});

// Actions
export const gameQuizActions = gameSlices.actions;

// // Selectors
export const selectQuestions = (state: RootState) => state.quiz.questions;
export const selectError = (state: RootState) => state.quiz.error;

export const selectScore = (state: RootState) => state.quiz.score;
export const selectAnswers = (state: RootState) => state.quiz.answers;

export const selectCurrentQuestionIndex = (state: RootState) =>
  state.quiz.currentQuestionIndex;

// Reducer

const gameQuizReducer = gameSlices.reducer;
export default gameQuizReducer;
