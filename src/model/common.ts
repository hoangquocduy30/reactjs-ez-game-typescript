export interface listResponse<T> {
  results: T[];
  response_code: number;
}
