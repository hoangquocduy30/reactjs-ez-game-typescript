import { Answers } from "../features/gameez/slices/gameSlices";
import { Quiz } from "./quiz";

export interface gamequiz {
  questions: Quiz[];
  error: string;
  score: number;
  currentQuestionIndex: number;
  answers: Answers[];
}
