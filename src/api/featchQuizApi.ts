import { listResponse } from "../model/common";
import { Quiz } from "./../model/quiz";
import axiosClient from "./axiosClient";

const featchQuizApi = {
  getAll(): Promise<listResponse<Quiz>> {
    return axiosClient.get("", {});
  },
};
export default featchQuizApi;
