import { all } from "redux-saga/effects";
import startGameSaga from "../features/gameez/saga/gameInitSaga";
import gameSaga from "../features/gameez/saga/gameSaga";

export default function* rootSaga() {
  console.log("rootSaga");
  yield all([startGameSaga(), gameSaga()]);
}
