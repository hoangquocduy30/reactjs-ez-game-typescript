export enum myConstants {
  START_GAME = "START_GAME",
  FETCHING_GAME_DATA = "FETCHING_GAME_DATA",
  GAME = "GAME",
  END_GAME = "END_GAME",
}
